﻿//Add New Comment  
$('#submited').on('click', function () {
    if ($('#CommentText').val() != "") {
        //setting up value to send in POST
        var com = $('#CommentText').val();
        var DiscussionID = $('#DiscussionID').val();
        var markers = { comment: com, ID: DiscussionID }

        var source = "/Discussions/AddComment"
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: source,
            data: { markers: JSON.stringify(markers) },
            success: Done,
            error: detectedError

        });
    }
    else {
        alert("Please enter a proper comment.")
    }
});
function Done(data) {
    console.log("------success----");
    //clears Text in Comment textbox
    $("#CommentText").text("")

    //data result
    var ComText = data["commentText"]
    var ComFirstName = data["firstName"]
    var ComLastName = data["lastName"]


    //set up tr element
    var head1 = $('<tr>')
    var body1 = $('<tr>')

    //place name and comment in th or td elements
    var name = $('<th>').text(ComFirstName + " " + ComLastName);
    var com = $('<td>', { height: "150" }).text(ComText);

    //place th and td element into tr elements
    head1.append(name)
    body1.append(com)

    //add new comment to Table
    $('.TableContent').prepend(body1);
    $('.TableContent').prepend(head1);

}
function detectedError(stuff) {
    console.log("error");
}