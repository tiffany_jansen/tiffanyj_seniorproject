﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(classproj.Startup))]
namespace classproj
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
