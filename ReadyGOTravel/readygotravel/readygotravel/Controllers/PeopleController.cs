﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using readygotravel.Models;

namespace readygotravel.Controllers
{
    [Authorize]
    public class PeopleController : Controller
    {
        private DBContext db = new DBContext();
        /*
        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }
        */

        // GET: People/Details/5
        public ActionResult Details()
        { 

            //Since this will be accesed from homepage we can't pass a people ID as parameter so we use the UserId to get the id of people relating to the logged in user.
            string idTemp = User.Identity.GetUserId();
            int? id = db.People.Where(n => n.UserID.Equals(idTemp)).First().PersonID;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            //Check if the logged in user is an Admin or not.
            if(IsAdminUser(User.Identity))
            {
                ViewBag.Admin = 1;
            }
            else
            {
                ViewBag.Admin = 0;
            }
            return View(person);
        }

        //Check if the logged in user is an Admin.
        public bool IsAdminUser(System.Security.Principal.IIdentity user)
        {
            //Do the stuff
            ApplicationDbContext userDB = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userDB));

            //Get all of the roles associated with the user.
            var roles = UserManager.GetRoles(user.GetUserId());
            //If the first one is Admin, then the user is an admin.
            if (roles.Count != 0)
            {
                if(roles[0] == "Admin")
                {
                    return true;
                }
            }
                return false;        
        }
        /*
        // GET: People/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonID,UserID,FirstName,LastName")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(person);
        }
        */

        // GET: People/Edit/5
        public ActionResult Edit()
        {
            //Since this will be accesed from homepage we can't pass a people ID as parameter so we use the UserId to get the id of people relating to the logged in user.
            string idTemp = User.Identity.GetUserId();
            int? id = db.People.Where(n => n.UserID.Equals(idTemp)).First().PersonID;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonID,UserID,FirstName,LastName")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "People");
            }
            return View(person);
        }

        /*
        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        */
    }
}
