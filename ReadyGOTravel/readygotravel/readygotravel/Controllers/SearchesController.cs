﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using readygotravel.Models;
using readygotravel.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace readygotravel.Controllers
{

    public class SearchesController : Controller
    {
        
        private DBContext db = new DBContext();
        private ApplicationDbContext userDB = new ApplicationDbContext();

        // GET: Searches
        public ActionResult Index()
        {
            var searches = db.Searches.Include(s => s.Person);
            return View(searches.ToList());
        }
        
        public ActionResult Result(int? id, decimal? left)
        {
            Result result = db.Results.Find(id);

            ViewBag.Remaining = left;
            ViewBag.Total = result.AvgFlightAmount + result.AvgHotelAmount + result.AvgFoodCost;
            //Makes sure that the average flight amount is not set to 0 or -1 as this would indicate errors
            if (result.AvgFlightAmount != (decimal)0.00 && result.AvgFlightAmount != (decimal)-1.00)
            {
                //Compare dollar amount available and update the ViewBag results accordingly. 
                if (left >= 0)
                {
                    ViewBag.Result = 0;
                    ViewBag.Message = "You can travel there for: ";
                    ViewBag.Message2 = "And you have this much remaining: ";
                }
                else if (left >= -500)
                {
                    ViewBag.Result = 1;
                    ViewBag.Message = "You can almost go if you had amount this much more: ";
                }
                else
                {
                    ViewBag.Result = 2;
                    ViewBag.Message = "You are too broke and can't go on that budget, you would need this much more: ";
                }

                
                //Check if the decimal value is 0, if so skip this.
                decimal num = RoundToFourths(result.AvgHotelStar);
                //Get the whole number of the rounded number
                decimal integral = Math.Truncate(num);
                //Get the decimal value of the rounded number
                decimal @decimal = num - integral;

                //Set the whole number and decimal to the View.
                ViewBag.Stars = integral;
                ViewBag.Decimal = @decimal;
            }
            else if(result.AvgFlightAmount == -1)
            {
                ViewBag.Result = 3;
                ViewBag.Message = "Oops your ending location was the same as your starting location!";
                ViewBag.Message2 = "Please update your search options and try again.";
            }
            else
            {
                ViewBag.Result = 4;
                ViewBag.Message = "No flights were available that matched your search criteria.";
                ViewBag.Message2 = "Please update your search options and try again.";
            }

            ViewBag.NumDays = (result.Search.EndDate - result.Search.StartDate).Days;

            //Present information to user
            return View(result);
        }

        //This method is TESTABLE!!
        public decimal RoundToFourths(decimal num)
        {
            bool positive = true;
            //if num is negative, make it positive and reset the bool value.
            if (num < 0)
            {
                num = num * (-1);
                positive = false;
            }

            decimal integral = Math.Truncate(num);
            decimal @decimal = num - integral;
                     
            //If the decimal value is less than 0.12 set the decimal value to 0.
            if (@decimal <= (decimal)0.12)
                @decimal = 0;
            //If the decimal value is less than 0.37 but bigger than 0.12 set the decimal value to 0.25.
            else if (@decimal <= (decimal)0.37)
                @decimal = (decimal)0.25;
            //If the decimal value is less than 0.62 but bigger than 0.37 set the decimal value to 0.5.
            else if (@decimal <= (decimal)0.62)
                @decimal = (decimal)0.5;
            //If the decimal value is less than 0.88 but bigger than 0.62 set the decimal value to 0.75.
            else if (@decimal <= (decimal)0.88)
                @decimal = (decimal)0.75;
            //If the decimal value is bigger than 0.88 set the decimal value to 1.
            else
                @decimal = 1;

            if(@decimal == 1)
            {
                integral++;
                @decimal--;
            }

            //If the number was negative, switch it back to negative.
            if (!positive)
            {
                return (-1) * (integral + @decimal);
            }

            return integral + @decimal;
        }

        // GET: Searches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Search search = db.Searches.Find(id);
            if (search == null)
            {
                return HttpNotFound();
            }
            return View(search);
        }

        // GET: Searches/Create
        public ActionResult Create()
        {
            //Create the list of states
            List<string> states = new List<string>();
            states.Add("---");
            foreach(string state in db.States.Select(s => s.StateName).ToList())
            {
                states.Add(state);
            }
            ViewBag.States = states;

            //Create the list of stars
            List<string> stars = new List<string>();
            stars.Add("No Preference");
            stars.Add("0-1");
            stars.Add("1-2");
            stars.Add("2-3");
            stars.Add("3-4");
            stars.Add("4-5");
            ViewBag.Stars = stars;

            //Create the list of flight types
            List<string> types = new List<string>();
            types.Add("Economy");
            //Types.Add("First Class");
            ViewBag.Flight = types;

            return View();
        }

        // POST: Searches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StartDate,EndDate,NumTravelers,MaxAmount")] SearchDashBoardVM searchVM, int? sAirport, int? eAirport, string HotelStarValue, string FlightType)
        {          
            //Get the username of the user that is logged in.
            string userName = User.Identity.Name;

            //Find the userID for the person logged in.
            string userID = userDB.Users
                .Where(u => u.UserName == userName)
                .Select(un => un.Id).FirstOrDefault();

            //Find the personID for the person logged in.
            int? personID = db.People
                .Where(p => p.UserID == userID)
                .Select(u => u.PersonID).FirstOrDefault();

            int id = -5;

            //If there is no user logged in, set the userID to -1, otherwise set it to the user logged in.
            if (personID == null || personID == 0)
            {
                id = -1;
            }
            else
            {
                id =  (int)personID;
            }

            //Find the corresponding numbers or symbols for the given inputs of the star value and flight type.
            int star = GetHotelStarValue(HotelStarValue);
            string type = GetFlightType(FlightType);
            
            if(sAirport == null | eAirport == null)
            {
                //Create the list of states
                List<string> stateList = new List<string>();
                stateList.Add("---");
                foreach (string state in db.States.Select(s => s.StateName).ToList())
                {
                    stateList.Add(state);
                }
                ViewBag.States = stateList;

                //Create the list of stars
                List<string> starList = new List<string>();
                starList.Add("No Preference");
                starList.Add("0-1");
                starList.Add("1-2");
                starList.Add("3-4");
                starList.Add("4-5");
                ViewBag.Stars = starList;

                //Create the list of flight types
                List<string> typeList = new List<string>();
                typeList.Add("Economy");
                //TypeList.Add("First Class");
                ViewBag.Flight = typeList;

                return View();
            }

            //Create the search (hopefully...)
            Search search = new Search { UserID = id, NumTravelers = searchVM.NumTravelers, StartDate = searchVM.StartDate, EndDate = searchVM.EndDate, MaxAmount = searchVM.MaxAmount, StartAirport = (int)sAirport, EndAirport = (int)eAirport, HotelStarValue = star, FlightType = type };

            if (ModelState.IsValid)
            {                               
                //Get the starting airport code from the DB
                string startAirPort = db.Airports
                    .Where(a => a.AirportID == sAirport)
                    .Select(a => a.AirportCode)
                    .FirstOrDefault();

                //Get the airport code for the ending airport
                string endAirPort = db.Airports
                    .Where(a => a.AirportID == eAirport)
                    .Select(a => a.AirportCode)
                    .FirstOrDefault();

                //Get the foodCost based on the ending state
                decimal foodCost = Math.Round(db.Airports
                    .Where(a => a.AirportID == search.EndAirport)
                    .Select(a => a.State.FoodCost)
                    .FirstOrDefault(), 2);

                //Determine total days on vacation
                TimeSpan totalDays = search.EndDate - search.StartDate;

                //Calcate foodCost by mulitiplying # of Days * # of Travelers * food cost average by state per day per person
                decimal totalFoodCost = Math.Round(foodCost * search.NumTravelers * Convert.ToDecimal(totalDays.TotalDays), 2);

                //Do the flight API stuff and get the average cost of flights.
                decimal flightInfo = GetFlightData(search.StartDate, search.EndDate, search.NumTravelers, startAirPort, endAirPort, type);

                //Do the hotel API stuff and get the average cost of hotels.                
                List<decimal> hotelInfo = GetHotelData(search.StartDate, search.EndDate, search.NumTravelers, endAirPort, star);
                decimal hotelCost = hotelInfo.First();
                decimal avgStar = hotelInfo.LastOrDefault();

                //Calculate how much money is left over
                decimal amount = GetRemainingAmount(search, flightInfo, hotelCost, totalFoodCost);

                //Save Search to DB
                db.Searches.Add(search);
                db.SaveChanges();

                //Save Result to DB
                Result result = new Result { SearchID = search.SearchID, AvgFlightAmount = flightInfo, AvgHotelAmount = hotelCost, AvgFoodCost = totalFoodCost, AvgHotelStar = avgStar };
                db.Results.Add(result);
                db.SaveChanges();

                //Now pass results as decimals to the results action
                return RedirectToAction("Result", "Searches", new { id = result.ResultID, left = amount });
            }

            //Create the list of states
            List<string> states = new List<string>();
            states.Add("---");
            foreach (string state in db.States.Select(s => s.StateName).ToList())
            {
                states.Add(state);
            }
            ViewBag.States = states;

            //Create the list of stars
            List<string> stars = new List<string>();
            stars.Add("No Preference");
            stars.Add("0-1");
            stars.Add("1-2");
            stars.Add("2-3");
            stars.Add("3-4");
            stars.Add("4-5");
            ViewBag.Stars = stars;

            //Create the list of flight types
            List<string> types = new List<string>();
            types.Add("Economy");
            //Types.Add("First Class");
            ViewBag.Flight = types;

            return View(searchVM);
        }

        private decimal GetRemainingAmount(Search search, decimal flight, decimal hotel, decimal food)
        {
            decimal amount = search.MaxAmount;
            if (search.MaxAmount < 0)
            {
                amount = amount + (flight + hotel + food);
                amount = Math.Round(amount, 2);
            }
            else
            {
                amount = amount - (flight + hotel + food);
                amount = Math.Round(amount, 2);
            }
            return amount;
        }

        //This Method is TESTABLE!!

        private int GetHotelStarValue(string HotelStarValue)
        {
            //Split the string into pieces
            string[] stars = HotelStarValue.Split('-');
            string last = stars.Last();
            //If the user selected a number sequence convert the number to an int
            if (int.TryParse(last, out int n))
            {
                return Convert.ToInt32(last);                
            }
            //Else return 0
            else
            {
                return 0;
            }
        }

        private string GetFlightType(string FlightType)
        {
            return "E";
        }

        private decimal GetFlightData(DateTime startDate, DateTime endDate, int travelers, string startAirPort, string endAirPort, string type)
        {
            //Bild URL for GET request to Airport API, including my key and url parameters
            string flightID = System.Web.Configuration.WebConfigurationManager.AppSettings["flightID"];
            string flightKey = System.Web.Configuration.WebConfigurationManager.AppSettings["flightKey"];
            string urlInfo = "http://developer.goibibo.com/api/search/?app_id=" + flightID + "&app_key=" + flightKey + "&format=json&source="
                + startAirPort + "&destination=" + endAirPort + "&dateofdeparture=" +
                startDate.Year + startDate.Month.ToString("00") + startDate.Day.ToString("00") + "&dateofarrival=" +
                endDate.Year + endDate.Month.ToString("00") + endDate.Day.ToString("00") + "&seatingclass=" + type + "&adults=" +
                travelers + "&children=0&infants=0&counter=0";

            //Get the response string by accessing the site and reading it all the way through.
            string responseString = GetResponseString(urlInfo);

            //Convert string into a json object
            var results = JsonConvert.DeserializeObject<dynamic>(responseString);

            //Int for totalling flights
            int sum = 0;

            //If the results returned data and has values
            try
            {
                if (results.data.onwardflights.HasValues)
                {
                    //Counter
                    int i = 0;
                    //Loop through the array inside json
                    foreach (var value in results.data.onwardflights)
                    {
                        //Add flight data and taxes to the total sum for all flights
                        sum = sum + Convert.ToInt32(results.data.onwardflights[i].fare.totalfare)
                            + Convert.ToInt32(results.data.onwardflights[i].fare.totaltaxes);
                        i++;
                    }
                    //Divide by the total number of flights to get an average
                    sum = sum / results.data.onwardflights.Count;
                }
            }
            catch
            {
                //This means the API returned an error, most likely from same start and end location
                sum = -1;
            }

            decimal cost = Convert.ToDecimal(sum);

            //Sum will be 0 if there were no flights available, -1 if there was an error
            if (sum != 0 && sum != -1)
            {
                cost = cost / 100;
            }
            //Return the average, rounded to 2 decimal places.
            return Math.Round(cost, 2);
        }

        public List<decimal> GetHotelData(DateTime startDate, DateTime endDate, int travelers, string location, int? star)
        {
            //Min number of rooms is 1.
            int numRooms = 1;
            //Check for "eveness"
            if(travelers%2 == 0)
            {
                //Then just divide by 2.
                numRooms = travelers / 2;
            }
            else
            {
                //Else, add 1 then divide.
                numRooms = (travelers + 1) / 2;
            }

            //Get the API Key fromt the AppSecrets File.
            string hotelKey = System.Web.Configuration.WebConfigurationManager.AppSettings["hotelKey"];

            //Construct the Url for the hotel API
            string hotelURL = "http://api.hotwire.com/v1/search/hotel?apikey=" + hotelKey + "&dest=" + location + "&rooms=" + numRooms + "&adults=" + travelers + "&children=0&startdate=" + startDate.Month.ToString("00") + "/" + startDate.Day.ToString("00") + "/" + startDate.Year + "&enddate=" + endDate.Month.ToString("00") + "/" + endDate.Day.ToString("00") + "/" + startDate.Year;

            //Get the response string from the site so I can start accessing it.
            string responseString = GetResponseString(hotelURL);
            
            //Turn the string into XML Format.
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseString);

            //Get all the elements by the "total price" tag.
            XmlNodeList priceList = doc.GetElementsByTagName("TotalPrice");
            XmlNodeList starList = doc.GetElementsByTagName("StarRating");

            //Make things easy to access.
            decimal totalPrice = 0;
            decimal totalStar = 0;
            List<decimal> retList = new List<decimal>();

            //Check if star value passed in is null or 0.
            if (star == null | star == 0)
            {
                //Average out all the prices and star ratings and return that average.                
                if (priceList.Count == 0)
                {
                    retList.Add(0);
                    return retList;
                }
                for (int i = 0; i < priceList.Count; i++)
                {
                    decimal price = Convert.ToDecimal(priceList.Item(i).InnerXml);
                    decimal starValue = Convert.ToDecimal(starList.Item(i).InnerXml);
                    totalPrice += price;
                    totalStar += starValue;
                }
                //Round price to 2 decimal places so it's actually a price.
                decimal hotelPrice = Math.Round((totalPrice / priceList.Count), 2);
                retList.Add(hotelPrice);

                //Round starValue to 2 decimal places so it's cleaner.
                decimal StarAvg = Math.Round((totalStar / starList.Count), 2);
                retList.Add(StarAvg);

                return retList;
            }
            //Check the starList to the starvalue passed in.
            else
            {
                //counter
                int j = 0;

                for (int i = 0; i < starList.Count; i++)
                {
                    decimal starValue = Convert.ToDecimal(starList.Item(i).InnerXml);
                    if(starValue >= star - 1 && starValue <= star)
                    {
                        decimal price = Convert.ToDecimal(priceList.Item(i).InnerXml);
                        decimal stars = Convert.ToDecimal(starList.Item(i).InnerXml);
                        totalPrice += price;
                        totalStar += stars;
                        j++;
                    }
                }
                if(j == 0)
                {
                    retList.Add(0);
                    return retList;
                }
                //Round price to 2 decimal places so it's actually a price.
                decimal hotelPrice = Math.Round((totalPrice / j), 2);
                retList.Add(hotelPrice);

                //Round starValue to 2 decimal places so it's cleaner.
                decimal StarAvg = Math.Round((totalStar / j), 2);
                retList.Add(StarAvg);

                //return the list
                return retList;
            }
        }

        private string GetResponseString(string url)
        {
            //Make a request using my urlInfo, then grab response information 
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream information = response.GetResponseStream();
            StreamReader reader = new StreamReader(information);

            //Grab full response information, read to the end of the data and put it into a string
            return reader.ReadToEnd();
        }

        /*
         * The method for the javascript to have access to the locations for the state.
         */
         [HttpGet]
        public JsonResult Regions(string id)
        {
            Debug.WriteLine("value(Controller) = " + id);

            //get the list of the airports from the given state.
            List<StateAirport> airports = db.Airports
                .Where(a => a.State.StateName == id)
                .Select(a => new StateAirport { AirportID = a.AirportID, Location = a.Location, StateName = a.State.StateName, AirportCode = a.AirportCode})
                .ToList();

            //turn the list into a json object.
            string result = JsonConvert.SerializeObject(airports, Newtonsoft.Json.Formatting.None);

            //return the json objext.
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //// GET: Searches/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Search search = db.Searches.Find(id);
        //    if (search == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.UserID = new SelectList(db.People, "PersonID", "UserID", search.UserID);
        //    return View(search);
        //}

        //// POST: Searches/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "SearchID,UserID,StartDate,EndDate,NumTravelers,MaxAmount,StartState,EndState")] Search search)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(search).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.UserID = new SelectList(db.People, "PersonID", "UserID", search.UserID);
        //    return View(search);
        //}

        //// GET: Searches/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Search search = db.Searches.Find(id);
        //    if (search == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(search);
        //}

        //// POST: Searches/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Search search = db.Searches.Find(id);
        //    db.Searches.Remove(search);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        /// <summary>
        /// This will just set a viewBag for searches count and then just return the DeleteAll view.
        /// </summary>
        /// <returns>A DeleteAll view.</returns>
        [Authorize]
        public ActionResult DeleteAll()
        {
            //Get logged in users id.
            string id = User.Identity.GetUserId();

            //Get peopleID from UserId.
            int peopleID = db.People.Where(n => n.UserID.Equals(id)).Select(n => n.PersonID).First();

            //Get searches done by peopleID.
            var searches = db.Searches.Where(n => n.UserID == peopleID).ToList();

            ViewBag.searchesCount = searches.Count();
            return View();
        }

        /// <summary>
        /// This will delete all searches and results relating to the user that is signed in.
        /// </summary>
        /// <returns>It will just return a redirect to the Searches/UserSearches.cshtml page.</returns>
        [Authorize]
        [HttpPost, ActionName("DeleteAll")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAllConfirmed()
        {
            //Get logged in users id.
            string id = User.Identity.GetUserId();

            //Get peopleID from UserId.
            int peopleID = db.People.Where(n => n.UserID.Equals(id)).Select(n => n.PersonID).First();

            //Get searches done by peopleID.
            var searches = db.Searches.Where(n => n.UserID == peopleID).ToList();

            var results = db.Results.ToList();

            //Deletes everyting in the list of searches for a user.
            for (int i = 0; i < searches.Count(); i++)
            {
                //The result associated with a search will need to be deleted before a search.
                db.Results.Remove(results.Where(n=> n.SearchID == searches[i].SearchID).First());
                db.Searches.Remove(searches[i]);
            }

            db.SaveChanges();
            return RedirectToAction("UserSearches", "Searches");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This will get the list of searches done by the current user and then pass them to the view to display them.
        /// </summary>
        /// <returns>The UserSearches view with a list of searches</returns>
        [Authorize]
        public ActionResult UserSearches()
        {
            //Get logged in users id.
            string id = User.Identity.GetUserId();

            //Get peopleID from UserId.
            int peopleID = db.People.Where(n => n.UserID.Equals(id)).Select(n => n.PersonID).First();

            //Get searches done by peopleID.
            var searches = db.Searches.Where(n => n.UserID == peopleID).OrderByDescending(n => n.SearchID);

            //Have the total number of searches.
            ViewBag.TotalSearches = searches.Count();

            return View(searches.Take(3).ToList());
        }

        /// <summary>
        /// This will just gather all search related information from a user and then return it as a JsonResult.
        /// </summary>
        /// <param name="id">This is the starting point of the next 10 searches in relation to the database.</param>
        /// <returns>A JsonResult with the information from a list of searches.</returns>
        [Authorize]
        public JsonResult AddSearches(int? id)
        {

            int check = id ?? default(int);

            //Get logged in users id.
            string id2 = User.Identity.GetUserId();

            //Get peopleID from UserId.
            int peopleID = db.People.Where(n => n.UserID.Equals(id2)).Select(n => n.PersonID).First();

            //Get searches done by peopleID.
            var moreSearches = db.Searches.Where(n => n.UserID == peopleID).OrderByDescending(n => n.SearchID).Skip(check).Take(10);


            //Setting up lists involved in displaying.
            var startDates = moreSearches.Select(n => n.StartDate.ToString()).ToList();
            var endDates = moreSearches.Select(n => n.EndDate.ToString()).ToList();
            var numTravelers = moreSearches.Select(n => n.NumTravelers).ToList();
            var maxAmounts = moreSearches.Select(n => n.MaxAmount).ToList();
            var startAirports = moreSearches.Select(n => n.Airport).Select(n => n.State).Select(n => n.StateName).ToList();
            var endAirports = moreSearches.Select(n => n.Airport1).Select(n => n.State).Select(n => n.StateName).ToList();
            var startAirportCodes = moreSearches.Select(n => n.Airport).Select(n => n.AirportCode).ToList();
            var endAirportCodes = moreSearches.Select(n => n.Airport1).Select(n => n.AirportCode).ToList(); 
            //Direction in state(N,E,S,W ..).
            var startAirportLocations = moreSearches.Select(n => n.Airport).Select(n => n.Location).ToList();
            //Direction in state(N,E,S,W ..).
            var endAirportLocations = moreSearches.Select(n => n.Airport1).Select(n => n.Location).ToList();
            var hotelStarValues = moreSearches.Select(n => n.HotelStarValue).ToList();

            //Data that will be returned to javascript file.
            var data = new { StartDates = startDates, EndDates = endDates, NumTravelers = numTravelers, MaxAmounts = maxAmounts, StartAirports = startAirports, EndAirports = endAirports, StartAirportCodes = startAirportCodes, EndAirportCodes = endAirportCodes, StartAirportLocations = startAirportLocations, EndAirportLocations = endAirportLocations, HotelStarValues = hotelStarValues};

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
