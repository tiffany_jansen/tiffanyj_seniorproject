﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace readygotravel.Models.ViewModels
{
    public class StateAirport
    {
        public int AirportID { get; set; }

        [Required]
        [StringLength(5)]
        public string AirportCode { get; set; }

        [StringLength(5)]
        public string Location { get; set; }

        public string StateName { get; set; }
    }
}