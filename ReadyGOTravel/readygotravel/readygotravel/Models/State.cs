namespace readygotravel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class State
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public State()
        {
            Airports = new HashSet<Airport>();
            Climates = new HashSet<Climate>();
            Geographies = new HashSet<Geography>();
        }

        public int StateID { get; set; }

        [Required]
        public string StateName { get; set; }

        public int CountryID { get; set; }

        [Column(TypeName = "money")]
        public decimal FoodCost { get; set; }

        [Required]
        public string FoodItemOne { get; set; }

        [Required]
        public string FoodItemTwo { get; set; }

        [Required]
        public string FoodItemThree { get; set; }

        public int SpringTemp { get; set; }

        public int SummerTemp { get; set; }

        public int FallTemp { get; set; }

        public int WinterTemp { get; set; }

        [Required]
        [StringLength(32)]
        public string SpringWeather { get; set; }

        [Required]
        [StringLength(32)]
        public string SummerWeather { get; set; }

        [Required]
        [StringLength(32)]
        public string FallWeather { get; set; }

        [Required]
        [StringLength(32)]
        public string WinterWeather { get; set; }

        public decimal CrimeRate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Airport> Airports { get; set; }

        public virtual Country Country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Climate> Climates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Geography> Geographies { get; set; }
    }
}
