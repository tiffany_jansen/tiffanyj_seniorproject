﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using readygotravel.Controllers;

namespace rgt_testing
{
    //PBI: 266 Sprint 3 Tests
    [TestClass]
    public class TiffanyTests
    {
        [TestMethod]
        [DataRow(2.1D, 2.0D)]
        [DataRow(2.2D, 2.25D)]
        [DataRow(2.3D, 2.25D)]
        [DataRow(2.4D, 2.5D)]
        [DataRow(2.5D, 2.5D)]
        [DataRow(2.6D, 2.5D)]
        [DataRow(2.7D, 2.75D)]
        [DataRow(2.8D, 2.75D)]
        [DataRow(2.9D, 3D)]
        public void RoundToFourths__ShouldRoundTenthsToNearestQuarter_True(double test, double expected)
        {
            //Arrange -> Convert String Values to Decimal Values
            decimal testDecimal = Convert.ToDecimal(test);
            decimal expectedDecimal = Convert.ToDecimal(expected);      

            //Act -> Run the method and catch the return value.
            decimal returnedDecimal = RoundToFourths(testDecimal);

            //Assert
            Assert.AreEqual(expectedDecimal, returnedDecimal);
        }

        [TestMethod]
        [DataRow(2.11D, 2.0D)]
        [DataRow(2.12D, 2.0D)]
        [DataRow(2.13D, 2.25D)]
        [DataRow(2.14D, 2.25D)]
        [DataRow(2.15D, 2.25D)]
        [DataRow(2.35D, 2.25D)]
        [DataRow(2.36D, 2.25D)]
        [DataRow(2.37D, 2.25D)]
        [DataRow(2.38D, 2.5D)]
        [DataRow(2.39D, 2.5D)]
        [DataRow(2.59D, 2.5D)]
        [DataRow(2.61D, 2.5D)]
        [DataRow(2.62D, 2.5D)]
        [DataRow(2.63D, 2.75D)]
        [DataRow(2.64D, 2.75D)]
        [DataRow(2.65D, 2.75D)]
        [DataRow(2.85D, 2.75D)]
        [DataRow(2.86D, 2.75D)]
        [DataRow(2.87D, 2.75D)]
        [DataRow(2.88D, 2.75D)]
        [DataRow(2.89D, 3D)]
        [DataRow(2.91D, 3D)]
        [DataRow(2.92D, 3D)]
        public void RoundToFourths__ShouldRoundHundredthsToNearestQuarter_True(double test, double expected)
        {
            //Arrange -> Convert String Values to Decimal Values
            decimal testDecimal = Convert.ToDecimal(test);
            decimal expectedDecimal = Convert.ToDecimal(expected);

            //Act -> Run the method and catch the return value.
            decimal returnedDecimal = RoundToFourths(testDecimal);

            //Assert
            Assert.AreEqual(expectedDecimal, returnedDecimal);
        }

        [TestMethod]
        [DataRow(-2.1D, -2.0D)]
        [DataRow(-2.2D, -2.25D)]
        [DataRow(-2.3D, -2.25D)]
        [DataRow(-2.4D, -2.5D)]
        [DataRow(-2.5D, -2.5D)]
        [DataRow(-2.6D, -2.5D)]
        [DataRow(-2.7D, -2.75D)]
        [DataRow(-2.8D, -2.75D)]
        [DataRow(-2.9D, -3D)]
        public void RoundToFourths__ShouldRoundNegativesToNearestQuarter_True(double test, double expected)
        {
            //Arrange -> Convert String Values to Decimal Values
            decimal testDecimal = Convert.ToDecimal(test);
            decimal expectedDecimal = Convert.ToDecimal(expected);

            //Act -> Run the method and catch the return value.
            decimal returnedDecimal = RoundToFourths(testDecimal);

            //Assert
            Assert.AreEqual(expectedDecimal, returnedDecimal);
        }

        [TestMethod]
        [DataRow(2.1D, 2.25D)]
        [DataRow(2.2D, 2.5D)]
        [DataRow(2.3D, 2.5D)]
        [DataRow(2.4D, 2.75D)]
        [DataRow(2.5D, 2.75D)]
        [DataRow(2.6D, 2.75D)]
        [DataRow(2.7D, 3D)]
        [DataRow(2.8D, 3D)]
        [DataRow(2.9D, 3.25D)]
        public void RoundToFourths__ShouldRoundTenthsToNearestQuarter_False(double test, double notExpected)
        {
            //Arrange -> Convert String Values to Decimal Values
            decimal testDecimal = Convert.ToDecimal(test);
            decimal notExpectedDecimal = Convert.ToDecimal(notExpected);

            //Act -> Run the method and catch the return value.
            decimal returnedDecimal = RoundToFourths(testDecimal);

            //Assert
            Assert.AreNotEqual(notExpectedDecimal, returnedDecimal);
        }


        //The method I am testing
        private decimal RoundToFourths(decimal num)
        {
            bool positive = true;
            if (num < 0)
            {
                num = num * (-1);
                positive = false;
            }
            
            decimal integral = Math.Truncate(num);
            decimal @decimal = num - integral;

            if (@decimal <= (decimal)0.12)
                @decimal = 0;
            else if (@decimal <= (decimal)0.37)
                @decimal = (decimal)0.25;
            else if (@decimal <= (decimal)0.62)
                @decimal = (decimal)0.5;
            else if (@decimal <= (decimal)0.88)
                @decimal = (decimal)0.75;
            else
                @decimal = 1;

            if (@decimal == 1)
            {
                integral++;
                @decimal--;
            }

            if (!positive)
            {
                return (-1) * (integral + @decimal);
            }
            return integral + @decimal;
        }
    }
}
